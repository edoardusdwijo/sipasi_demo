<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/dashboard.css">

	<title>Dashboard</title>
</head>
<body>
	<div class="hitam_putih">
	<!-- start navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark">
	  <div class="container">
	    <a class="navbar-brand" href="./">
	    	<img src="image/logo.png" alt="30" width="30" height="" class="d-inline-block align-text-top">
      Kelurahan Tanjung
	    </a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarNav">
	      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="nav-link text-light" aria-current="page" href="user/informasi/">Informasi</a>
	        </li>
	        <li class="nav-item">
	          <input type="button" onclick="document.location='login/'" value="Login" class="tombol">
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<!-- end navbar -->
	
	<!-- start landing -->
	<div class="container">
	  <div class="row justify-content-between">
	    <div class="col-lg-7 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
	      <div class="tampil">
	      	<h1>Ajukan Pengaduan atau Aspirasi Anda</h1>
            <h2>Laporkan kepada kami jika ada Pengaduan ataupun Aspirasi yang dapat menjadi langkah awal untuk kita menjadi lebih baik lagi.</h2>
            <div class="text-center text-lg-start">
              <a href="login/" class="pengaduan">Buat Pengaduan Baru</a>
            </div>
          </div>
	      </div>
	    </div>
	  </div>
	<!-- start landing -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>