<?php
	$koneksi = mysqli_connect("localhost", "root", "", "db_sipasi");

	//membuat query data ke database
	function query($query){
		global $koneksi;
		$ambil = mysqli_query($koneksi, $query);
		$rows = [];
		while ($row = mysqli_fetch_assoc($ambil)) {
			$rows[] = $row;
		}
		return $rows;
	}

	//untuk registrasi
	function registrasi($data){
		global $koneksi;

		$username = strtolower(stripcslashes($data["username"]));
		$nama_lengkap = htmlspecialchars($data["nama_lengkap"]);
		$password = mysqli_real_escape_string($koneksi,$data["password"]);
		$konfirmasi = mysqli_real_escape_string($koneksi,$data["konfirmasi"]);
		$level = htmlspecialchars($data["level"]);

	//cek username
		$result = mysqli_query($koneksi, "SELECT username FROM tb_user WHERE username='$username'");
		if (mysqli_fetch_assoc($result)) {
			echo "
				<script>
					alert('Username Sudah Terdaftar, Silahkan Coba Lagi')
				</script>
			";
			return false;
		}

	//cek konfirmasi password
		if ($password !== $konfirmasi) {
			echo "<script>
				alert('Konfirmasi Password Tidak Sesuai, Coba Lagi');
				</script>";
			return false;
		}

	//enkripsi password
		$password = md5($password);	

	//tambah user baru
		mysqli_query($koneksi, "INSERT INTO tb_user values 
			('', '$username','$password', '$nama_lengkap', '$level')");


			return mysqli_affected_rows($koneksi);
	}

	//kita buat fungsi tambah

	function simpan($data){
		global $koneksi;

		$kategori = htmlspecialchars($data["kategori"]);
		$username = htmlspecialchars($data["username"]);
		$subjek = htmlspecialchars($data["subjek"]);
		$pengaduan = htmlspecialchars($data["pengaduan"]);
		$tanggapan = htmlspecialchars($data["tanggapan"]);
		$status = htmlspecialchars($data["status"]);

	//buat upload file
		$foto = upload();
			if (!$foto) {
				return false;
			}

		$query = "INSERT INTO tb_aspirasi
			values
		('', '$username', '$kategori', '$subjek', '$pengaduan', '$foto', '$tanggapan', '$status')";

		mysqli_query($koneksi, $query);

		return mysqli_affected_rows($koneksi);
	}

	function upload(){

	//ambildulu file yang di kirim dengan $_FILES

		$namaFile = $_FILES['foto']['name'];
		$ukuranFile = $_FILES['foto']['size'];
		$error = $_FILES['foto']['error'];
		$tmpName = $_FILES['foto']['tmp_name'];

	//cek apakah tidak ada gambar yg di upload
		if ($error === 4) {
			echo " <script>
					alert ('pilih file dahulu');
					</script>";
			return false;
		}

		//cek apakah yang di upload gambar atau bukan
		$ekstensiFotoValid = ['jpg', 'jpeg', 'png'];
		$ekstensiFoto = explode('.', $namaFile);
		$ekstensiFoto = strtolower(end ($ekstensiFoto));

		if (!in_array($ekstensiFoto, $ekstensiFotoValid)) {
			echo " <script>
					alert ('yang anda upload bukan file');
					</script>";
			return false;
		}

	// cek jika ukuran terlalu besar
		if ($ukuranFile > 2000000) {
			echo " <script>
					alert ('ukuran terlalu besar, ganti yang lain atau ubah ukuran');
				</script>";
			return false;
		}

	//jika upload lolos
	//generate nama file baru

		$namaFileBaru = uniqid();
		$namaFileBaru .='.';
		$namaFileBaru.= $ekstensiFoto;


		move_uploaded_file($tmpName, '../../img_aspirasi/'.$namaFileBaru);

		return $namaFileBaru;


	}

	//kita buat fungsi edit data

	function ubah($data){
		global $koneksi;

		$id_aspirasi = $data["id_aspirasi"];
		$tanggapan = htmlspecialchars($data["tanggapan"]);
		$status = htmlspecialchars($data["status"]);

		$query = "UPDATE tb_aspirasi SET
		tanggapan = '$tanggapan',
		status = '$status'
		WHERE id_aspirasi = $id_aspirasi";

		mysqli_query($koneksi, $query);

		return mysqli_affected_rows($koneksi);
	}

	//kita buat fungsi hapus informasi

	function hapus($id_informasi){

		global $koneksi;
		mysqli_query($koneksi, "DELETE FROM tb_informasi where id_informasi = $id_informasi");


		return mysqli_affected_rows($koneksi);
	}

	//kita buat fungsi hapus aspirasi

	function hapus_aspirasi($id_aspirasi){

		global $koneksi;
		mysqli_query($koneksi, "DELETE FROM tb_aspirasi where id_aspirasi = $id_aspirasi");


		return mysqli_affected_rows($koneksi);
	}

	//fungsi cari
	function cari ($keyword){
		$query = "SELECT * FROM tb_aspirasi
					WHERE
					nama_lengkap like '%$keyword%' OR 
					kategori like '%$keyword%'
						OR 
					subjek like '%$keyword%'
						OR 
					pengaduan like '%$keyword%'
						OR 
					status like '%$keyword%'";
		return query($query);
}

?>