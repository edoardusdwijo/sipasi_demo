<?php
	$koneksi = mysqli_connect("localhost", "root", "", "db_sipasi");

	//membuat query data ke database
	function query($query){
		global $koneksi;
		$ambil = mysqli_query($koneksi, $query);
		$rows = [];
		while ($row = mysqli_fetch_assoc($ambil)) {
			$rows[] = $row;
		}
		return $rows;
	}

	//untuk registrasi
	function registrasi($data){
		global $koneksi;

		$username = strtolower(stripcslashes($data["username"]));
		$password = mysqli_real_escape_string($koneksi,$data["password"]);
		$konfirmasi = mysqli_real_escape_string($koneksi,$data["konfirmasi"]);
		$level = htmlspecialchars($data["level"]);

	//cek username
		$result = mysqli_query($koneksi, "SELECT username FROM tb_user WHERE username='$username'");
		if (mysqli_fetch_assoc($result)) {
			echo "
				<script>
					alert('Username Sudah Terdaftar, Silahkan Coba Lagi')
				</script>
			";
			return false;
		}

	//cek konfirmasi password
		if ($password !== $konfirmasi) {
			echo "<script>
				alert('Konfirmasi Password Tidak Sesuai, Coba Lagi');
				</script>";
			return false;
		}

	//enkripsi password
		$password = md5($password);	

	//tambah user baru
		mysqli_query($koneksi, "INSERT INTO tb_user values 
			('','$username','$password', '$level')");


			return mysqli_affected_rows($koneksi);
	}

	//kita buat fungsi tambah

	function simpan($data){
		global $koneksi;

		$judul_info = htmlspecialchars($data["judul_info"]);
		$tanggal_info = htmlspecialchars($data["tanggal_info"]);
		$isi_info = htmlspecialchars($data["isi_info"]);

	//buat upload file
		$gambar = upload();
			if (!$gambar) {
				return false;
			}

		$query = "INSERT INTO tb_informasi
			values
		('', '$judul_info', '$tanggal_info', '$isi_info', '$gambar')";

		mysqli_query($koneksi, $query);

		return mysqli_affected_rows($koneksi);
	}

	function upload(){

	//ambildulu file yang di kirim dengan $_FILES

		$namaFile = $_FILES['gambar']['name'];
		$ukuranFile = $_FILES['gambar']['size'];
		$error = $_FILES['gambar']['error'];
		$tmpName = $_FILES['gambar']['tmp_name'];

	//cek apakah tidak ada gambar yg di upload
		if ($error === 4) {
			echo " <script>
					alert ('pilih file dahulu');
					</script>";
			return false;
		}

		//cek apakah yang di upload gambar atau bukan
		$ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
		$ekstensiGambar = explode('.', $namaFile);
		$ekstensiGambar = strtolower(end ($ekstensiGambar));

		if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
			echo " <script>
					alert ('yang anda upload bukan file');
					</script>";
			return false;
		}

	// cek jika ukuran terlalu besar
		if ($ukuranFile> 500000) {
			echo " <script>
					alert ('ukuran terlalu besar, ganti yang lain atau ubah ukuran');
				</script>";
			return false;
		}

	//jika upload lolos
	//generate nama file baru

		$namaFileBaru = uniqid();
		$namaFileBaru .='.';
		$namaFileBaru.= $ekstensiGambar;


		move_uploaded_file($tmpName, '../../img_informasi/'.$namaFileBaru);

		return $namaFileBaru;


	}
?>