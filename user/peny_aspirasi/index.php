<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../../css/peny_aspirasi.css">

    <title>Penyampaian Aspirasi</title>
</head>
<body>
    <!-- start navbar -->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="../">
                <img src="../../image/logo.png" alt="30" width="30" height="" class="d-inline-block align-text-top">
                Kelurahan Tanjung
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="../">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="../peny_aspirasi/">Penyampaian Aspirasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../informasi/">Informasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../">logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end navbar -->

    <!-- start buat pengaduan -->
    <div class="container pt-4">
        <div class="judul">
            <h1>Buat Pengaduan</h1>
        </div>

        <div class="form">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Pilih Kategori Pengaduan</label><br>
                    <select class="form-select" aria-label="Default select example" name="kategori">
                        <option selected>Pilih Kategori</option>
                        <option value="Layanan Publik">Layanan Publik</option>
                        <option value="Tenaga Kerja">Tenaga Kerja</option>
                        <option value="Lingkungan">Lingkungan</option>
                        <option value="Lalu Lintas">Lalu Lintas</option>
                        <option value="Bencana">Bencana</option>
                        <option value="Kesehatan">Kesehatan</option>
                    </select>
                </div>

                <div class="mb-3">
                    <input type="hidden" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        name="username" value="Demo">
                </div>


                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Subjek Pengaduan</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Masukkan Subjek" name="subjek">
                </div>

                <div class="mb-3">
                    <label for="exampleFormControlTextarea1">Pengaduan</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                        placeholder="Ketik Pengaduan Disini" name="pengaduan"></textarea>
                </div>

                <div class="mb-3">
                    <label for="formFile" class="form-label">Lampirkan Foto</label>
                    <input class="form-control" type="file" id="formFile" name="foto">

                    <div class="mb-3">
                        <input type="hidden" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            name="tanggapan"
                            value="Hai <?php echo $_SESSION['nama']; ?>, Saat Ini Belum Ada Tanggapan Dari Petugas Kelurahan">
                    </div>

                    <div class="mb-3">
                        <input type="hidden" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            name="status" value="Belum Diproses">
                    </div>


                    <input type="button" name="kirim" value="Kirim" class="tmbl_kirim"
                        onclick="document.location='../'">
                </div>
            </form>
        </div>
    </div>
    <!-- end buat pengaduan -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
</html>