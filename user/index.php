<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/home_user.css">

    <title>Home</title>
</head>
<body>
    <!-- start navbar -->
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="./">
                <img src="../image/logo.png" alt="30" width="30" height="" class="d-inline-block align-text-top">
                Kelurahan Tanjung
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="./">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="peny_aspirasi/">Penyampaian Aspirasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="informasi/">Informasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end navbar -->

    <!-- start home -->
    <div class="hero">
        <div class="container">
            <div class="judul">
                <h2>Layanan Aspirasi dan Pengaduan Masyarakat Kelurahan Tanjung</h2>
                <p>Sampaikan laporan anda kepada petugas kelurahan</p>
                <h3>
                    Hai, Selamat Datang Di Sistem Informasi Penyampaian Aspirasi
                </h3>
            </div>
        </div>
    </div>

    <div class="tampil_pengaduan">
        <div class="container pt-4">
            <div class="pengaduan mb-4">
                <h1>Penyampaian Aspirasi</h1>
            </div>

            <form action="" method="post">
                <div class="d-flex flex-row-reverse mb-4">
                    <input class="btn btn-primary" type="submit" name="cari" value="Cari">
                    <input type="text" name="keyword" class="form-control" placeholder="Cari disini..."
                        autocomplete="off">
                </div>
            </form>

            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="https://placehold.co/600x400.png" class="img-fluid rounded-start" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h6 class="card-title">Pengirim Aspirasi : Demo</h6>
                            <h6 class="card-title">Kategori Pengaduan : Layanan Publik</h6>
                            <h6 class="card-title mb-3">Subjek Pengaduan : Lorem ipsum dolor sit</h6>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem eum
                                quibusdam qui deserunt fugit corporis perspiciatis inventore? Optio adipisci quibusdam
                                repudiandae omnis voluptas! A modi odit, dicta ea ipsum maiores.</p>
                            <p class="card-text"><small class="text-muted">Status :
                                    Sedang Diproses</small></p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h5>Tanggapan Dari Petugas Kelurahan :</h5>

                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </li>
                </ul>
            </div>

            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="https://placehold.co/600x400.png" class="img-fluid rounded-start" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h6 class="card-title">Pengirim Aspirasi : Demo</h6>
                            <h6 class="card-title">Kategori Pengaduan : Layanan Publik</h6>
                            <h6 class="card-title mb-3">Subjek Pengaduan : Lorem ipsum dolor sit</h6>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem eum
                                quibusdam qui deserunt fugit corporis perspiciatis inventore? Optio adipisci quibusdam
                                repudiandae omnis voluptas! A modi odit, dicta ea ipsum maiores.</p>
                            <p class="card-text"><small class="text-muted">Status :
                                    Sedang Diproses</small></p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h5>Tanggapan Dari Petugas Kelurahan :</h5>

                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </li>
                </ul>
            </div>
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="https://placehold.co/600x400.png" class="img-fluid rounded-start" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h6 class="card-title">Pengirim Aspirasi : Demo</h6>
                            <h6 class="card-title">Kategori Pengaduan : Layanan Publik</h6>
                            <h6 class="card-title mb-3">Subjek Pengaduan : Lorem ipsum dolor sit</h6>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem eum
                                quibusdam qui deserunt fugit corporis perspiciatis inventore? Optio adipisci quibusdam
                                repudiandae omnis voluptas! A modi odit, dicta ea ipsum maiores.</p>
                            <p class="card-text"><small class="text-muted">Status :
                                    Sedang Diproses</small></p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h5>Tanggapan Dari Petugas Kelurahan :</h5>

                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </li>
                </ul>
            </div>
            <br>

        </div>
    </div>
    <!-- end home -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
</html>