<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../../css/informasi.css">

    <title>Informasi</title>
</head>
<body>
    <!-- start navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="../">
                <img src="../../image/logo.png" alt="30" width="30" height="" class="d-inline-block align-text-top">
                Kelurahan Tanjung
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="../">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../peny_aspirasi/">Penyampaian Aspirasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="../informasi/">Informasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end navbar -->

    <!-- start informasi -->
    <div class="informasi">
        <div class="container pt-4">
            <div class="judul">
                <h1>Informasi</h1>
            </div>

            <div class="informasi mt-4">
                <div class="row">
                    <div class="col-md-8">

                        <div class="card mb-3">
                            <img src="https://placehold.co/600x400.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Lorem ipsum dolor sit amet.</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sed,
                                    sit minima exercitationem dolorum necessitatibus impedit id, velit odio quas rerum
                                    veniam, reprehenderit temporibus totam non. Omnis tempora illo inventore?</p>
                                <p class="card-text"><small class="text-muted">12.12.2021</small></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <img src="https://placehold.co/600x400.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Lorem ipsum dolor sit amet.</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sed,
                                    sit minima exercitationem dolorum necessitatibus impedit id, velit odio quas rerum
                                    veniam, reprehenderit temporibus totam non. Omnis tempora illo inventore?</p>
                                <p class="card-text"><small class="text-muted">12.12.2021</small></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <img src="https://placehold.co/600x400.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Lorem ipsum dolor sit amet.</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sed,
                                    sit minima exercitationem dolorum necessitatibus impedit id, velit odio quas rerum
                                    veniam, reprehenderit temporibus totam non. Omnis tempora illo inventore?</p>
                                <p class="card-text"><small class="text-muted">12.12.2021</small></p>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-4">
                        <div class="card mb-3">
                            <img src="https://placehold.co/600x400.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kelurahan Tanjung</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem magni
                                    veritatis voluptatum autem maiores? Recusandae maiores magnam tenetur omnis? Vel
                                    recusandae atque maiores tempore blanditiis dolorem dicta hic cumque voluptatibus!
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Alamat: -<br>
                                </li>
                                <li class="list-group-item">
                                    Kontak:<br>
                                    08123456789
                                </li>
                                <li class="list-group-item">
                                    Jadwal Operasional:<br>
                                    Senin : 07:30 - 15:00<br>
                                    Selasa : 07:30 - 15:00<br>
                                    Rabu : 07:30 - 15:00<br>
                                    Kamis: 07:30 - 15:00<br>
                                    Jumat: 07:30 - 15:00
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end informasi -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
</html>