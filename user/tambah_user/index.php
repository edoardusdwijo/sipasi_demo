<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../../css/daftar_user.css">

    <title>Daftar</title>
</head>
<body>
    <div class="hitam_putih">
        <!-- start navbar -->
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark">
            <div class="container">
                <a class="navbar-brand text-light" href="../../">
                    <img src="../../image/logo.png" alt="30" width="30" height="" class="d-inline-block align-text-top">
                    Kelurahan Tanjung
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link text-light" aria-current="page" href="../../">Dashboard</a>
                        </li>
                        <li>
                            <input type="button" onclick="document.location='../../login/'" value="Login"
                                class="tombol">
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- end navbar -->

        <!-- start form login -->
        <div class="form_login">
            <h1>Daftar</h1>
            <form action="" method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Username</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Masukkan Username" name="username">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Masukkan Nama Lengkap" name="nama_lengkap">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1"
                        placeholder="Masukkan Password" name="password">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Konfirmasi Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1"
                        placeholder="Masukkan Password" name="konfirmasi">
                </div>

                <div class="mb-3">
                    <input type="hidden" class="form-control" name="level" value="user">
                </div>
                <input type="button" name="daftar" value="Daftar" class="tombol_daftar"
                    onclick="document.location='../../login'" />

            </form>
        </div>
        <!-- end form login -->
    </div>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
</html>